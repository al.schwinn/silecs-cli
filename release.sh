#!/bin/bash
#
# release script
#

DEST_BASE=/common/usr/cscofe
DEST_SCRIPTS=${DEST_BASE}/scripts
DEST_FILE=${DEST_SCRIPTS}/silecs.sh
DEST_LINK=${DEST_BASE}/bin/silecs

cp silecs.sh ${DEST_SCRIPTS}
echo "Copied script to ${DEST_SCRIPTS}"

chmod +x ${DEST_FILE}
echo "Adjusted permissions of ${DEST_FILE}"

if [ ! -e  "${DEST_LINK}" ] ; then
  ln -s ${DEST_FILE} ${DEST_LINK}
  echo "Created softlink ${DEST_LINK}"
else
  echo "Softlink ${DEST_LINK} exists."
fi

echo "done"